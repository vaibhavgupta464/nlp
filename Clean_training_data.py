About Code : Code to clean training data for fasttext in custom to standard project


-----------------------------------------------------------------------------

import pandas as pd
import numpy as np
import re
from io import StringIO

df = pd.read_excel('C:/Users/imart/Desktop/custom to standard project/forpmcat 10573/custom_to_std_for_10573trainig.xlsx')


'''for i in range(0,26662):
 f = df.OPTIONS_DESC[i].isdigit()
'''



specials = ['!','/','-','$','#','@','^','*','(',')','+','%','\\','"',']','[','+','=','-','&']
for i in range(0,len(df)):
  
    
    list1=[]
    
    if(type(df.OPTIONS_DESC[i]) == float):
        df.set_value(i,'OPTIONS_DESC','')
        continue
    
    list1 = [item for item in df.OPTIONS_DESC[i].split() if item not in specials]
    
    str1=''.join(list1)
    
    
    if(str1.isdigit() == True):
    
            df.set_value(i,'OPTIONS_DESC','')
            continue
    str2 = ' '.join(list1)
    df.set_value(i,'OPTIONS_DESC',str2)
         
for p in range(0,len(df)):    
   if(df.OPTIONS_DESC[p].startswith("as per") or df.OPTIONS_DESC[p].startswith("As per") or df.OPTIONS_DESC[p] == "Text" or df.OPTIONS_DESC[p] == "text" or df.OPTIONS_DESC[p] == "any" or df.OPTIONS_DESC[p] == "Any"):
      df.set_value(p,'OPTIONS_DESC','') 
    
    
for k in range(0,len(df)):
    df.set_value(k,'OPTIONS_DESC',df.OPTIONS_DESC[k].strip())
    df.set_value(k,'MASTER_DESC',df.MASTER_DESC[k].strip())
    df.set_value(k,'MASTER_DESC1',df.MASTER_DESC1[k].strip())
    
for l in range(0,len(df)): 
    abc = df.OPTIONS_DESC[l]
    abc = abc.replace("-", " ")
    abc = abc.replace("/", " ")
    abc = abc.replace("!", " ")
    abc = abc.replace("#", " ")
    abc = abc.replace("@", " ")
    abc = abc.replace("$", " ")
    abc = abc.replace("%", " ")
    abc = abc.replace("^", " ")
    abc = abc.replace("&", " ")
    abc = abc.replace("*", " ")
    abc = abc.replace("(", " ")
    abc = abc.replace(")", " ")
    abc = abc.replace("+", " ")
    abc = abc.replace(";", " ")
    abc = abc.replace("{", " ")
    abc = abc.replace("}", " ")
    abc = abc.replace("[", " ")
    abc = abc.replace("]", " ")
    abc = abc.replace("\\", " ")
    abc = abc.replace(".", " ")
    abc = abc.replace(",", " ")
    abc = abc.replace(" x ", " ")
    abc = abc.replace("wxdxh","w d h")
    df.set_value(l,'OPTIONS_DESC',abc)
    abc = df.MASTER_DESC1[l]
    abc = abc.replace("-", " ")
    abc = abc.replace("/", " ")
    abc = abc.replace("!", " ")
    abc = abc.replace("#", " ")
    abc = abc.replace("@", " ")
    abc = abc.replace("$", " ")
    abc = abc.replace("%", " ")
    abc = abc.replace("^", " ")
    abc = abc.replace("&", " ")
    abc = abc.replace("*", " ")
    abc = abc.replace("(", " ")
    abc = abc.replace(")", " ")
    abc = abc.replace("+", " ")
    abc = abc.replace(";", " ")
    abc = abc.replace("{", " ")
    abc = abc.replace("}", " ")
    abc = abc.replace("[", " ")
    abc = abc.replace("]", " ")
    abc = abc.replace("\\", " ")
    abc = abc.replace(".", " ")
    abc = abc.replace(",", " ")
    df.set_value(l,'MASTER_DESC1',abc)
    abc = df.MASTER_DESC[l]
    abc = abc.replace("-", " ")
    abc = abc.replace("/", " ")
    abc = abc.replace("!", " ")
    abc = abc.replace("#", " ")
    abc = abc.replace("@", " ")
    abc = abc.replace("$", " ")
    abc = abc.replace("%", " ")
    abc = abc.replace("^", " ")
    abc = abc.replace("&", " ")
    abc = abc.replace("*", " ")
    abc = abc.replace("(", " ")
    abc = abc.replace(")", " ")
    abc = abc.replace("+", " ")
    abc = abc.replace(";", " ")
    abc = abc.replace("{", " ")
    abc = abc.replace("}", " ")
    abc = abc.replace("[", " ")
    abc = abc.replace("]", " ")
    abc = abc.replace("\\", " ")
    abc = abc.replace(".", " ")
    abc = abc.replace(",", " ")
    df.set_value(l,'MASTER_DESC',abc)
    
for r in range(0,len(df)):
    emails = re.findall(r'[0-9]x[0-9]',df.OPTIONS_DESC[r])
    if(emails == []):
        continue
    else:
        df.set_value(r,'OPTIONS_DESC',df.OPTIONS_DESC[r].replace("x", " "))
    

for s in range(0,len(df)):
    emails = re.findall(r' x[0-9]',df.OPTIONS_DESC[s])
    if(emails == []):
        continue
    else:
        df.set_value(s,'OPTIONS_DESC',df.OPTIONS_DESC[s].replace("x", " "))
        
for t in range(0,len(df)):
    emails = re.findall(r'[0-9]x ',df.OPTIONS_DESC[t])
    if(emails == []):
        continue
    else:
        df.set_value(t,'OPTIONS_DESC',df.OPTIONS_DESC[t].replace("x", " "))
    
    
for m in range(0,len(df)):
   df.set_value(m,'OPTIONS_DESC', df.OPTIONS_DESC[m].replace("    ", " "))
   df.set_value(m,'OPTIONS_DESC', df.OPTIONS_DESC[m].replace("   ", " "))
   df.set_value(m,'OPTIONS_DESC', df.OPTIONS_DESC[m].replace("  ", " "))
   df.set_value(m,'MASTER_DESC', df.MASTER_DESC[m].replace("   ", " "))
   df.set_value(m,'MASTER_DESC', df.MASTER_DESC[m].replace("  ", " "))
   df.set_value(m,'MASTER_DESC', df.MASTER_DESC[m].replace("__", "_"))
   df.set_value(m,'MASTER_DESC', df.MASTER_DESC[m].replace("___", "_"))
   df.set_value(m,'MASTER_DESC1', df.MASTER_DESC1[m].replace("   ", " "))
   df.set_value(m,'MASTER_DESC1', df.MASTER_DESC1[m].replace("  ", " "))
   df.set_value(m,'MASTER_DESC1', df.MASTER_DESC1[m].replace("____", "_"))
   df.set_value(m,'MASTER_DESC1', df.MASTER_DESC1[m].replace("___", "_"))
   df.set_value(m,'MASTER_DESC1', df.MASTER_DESC1[m].replace("__", "_"))
   

   
   
for n in range(0,len(df)):
    df.set_value(n,'OPTIONS_DESC',df.OPTIONS_DESC[n].strip())
    df.set_value(n,'MASTER_DESC',df.MASTER_DESC[n].strip())
    df.set_value(n,'MASTER_DESC1',df.MASTER_DESC1[n].strip())
    
    

    
df["Join"] = ""
 
for u in range(0,len(df)):
   print(u)
   df.Join[u]="".join(['__label__',str(df.MCAT_ID[u]),'_',df.MASTER_DESC1[u],' ',df.MASTER_DESC[u],' ',df.OPTIONS_DESC[u]])
   

df.to_excel('C:/Users/imart/Desktop/custom to standard project/forpmcat 10573/Traning_Data_Standard_ISQ_After_Cleaning.xlsx',index=False)


df1 = df
df1 = df1.drop_duplicates()
df1.reset_index(drop=True,inplace=True)



    
for z in range(0,len(df1)):
       text_file = open("C:/Users/imart/Desktop/custom to standard project/forpmcat 10573/Output/"+str(df1.MCAT_ID[z])+".txt", "a")
       text_file.write(df1.Join[z])
       text_file.write('\n')
       text_file.close()
