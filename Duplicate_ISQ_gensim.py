About Code : Python code to check duplicate ISQs in MCAT using Gensim library.




-------------------------------------------------------------------------------



import gensim
model = gensim.models.KeyedVectors.load_word2vec_format('C:/Users/imart/Downloads/GoogleNews-vectors-negative300.bin', binary=True) # requires 3 gb data to download
'''
try:
    s = model.similarity('diameter','thickness')
except:
    continue

'''


import pandas as pd

df1 = pd.read_csv('file:///C:/Users/imart/Desktop/Duplicate/Standard_ISQ.csv',encoding = "ISO-8859-1")   #id,isqs

df1.columns
df1.drop('GLCAT_MCAT_NAME', axis=1, inplace=True)
df1.columns = ['mcat','isq']
df1=df1.sort_values(by=["mcat"])
df1=df1.head(10000)
df1=df1[df1["MCAT_ID"]]

df1.reset_index(drop=True, inplace=True)

for m in range(0,len(df1)):
   df1.set_value(m,'isq', df1.isq[m].replace("X", ""))
   df1.set_value(m,'isq', df1.isq[m].replace("  ", " "))
   df1.set_value(m,'isq', df1.isq[m].replace("L B H", " "))
   
   
for n in range(0,len(df1)):
    df1.set_value(n,'isq',df1.isq[n].strip())
    df1.set_value(n,'isq',df1.isq[n].strip())
    
    
    
    
df9=pd.DataFrame(columns=['mcat','isq','matched_isq','gensim','similarity','method'])
i=0
j=0
mcatslist=list(df1['mcat'].unique())
row=0
#x=1
for i in mcatslist:
    isqlist=list(df1[df1['mcat']==i]['isq'].values)
    for j in range (0,len(isqlist)):
        y=1
        for x in range(j+1,len(isqlist)):
            wordFromList1=[]
            wordFromList2=[]
            
            try:
                percentage=0
                s = model.similarity(isqlist[j].lower(),isqlist[j+y].lower())
                if(s>=0.53):
                    df9.set_value(row,'mcat',i)
                    df9.set_value(row,"isq",isqlist[j])
                    df9.set_value(row,"matched_isq",isqlist[j+y])
                    df9.set_value(row,"method",'gensim method')
                    df9.set_value(row ,"similarity",percentage)
                    df9.set_value(row ,"gensim",s)
                
                
            except:
                s=0
                matched_characters = sum(c1 == c2 for c1, c2 in zip(isqlist[j].lower(),isqlist[j+y].lower()))
                if(len(isqlist[j]) > len(isqlist[j+y])):
                    percentage = matched_characters/len(isqlist[j])
                    
                else:
                    percentage = matched_characters/len(isqlist[j+y])
                    
                if(percentage>=0.85):
                    df9.set_value(row,'mcat',i)
                    df9.set_value(row,"isq",isqlist[j])
                    df9.set_value(row,"matched_isq",isqlist[j+y])
                    df9.set_value(row,"method",'Simlarity Method')
                    df9.set_value(row ,"similarity",percentage)
                    df9.set_value(row ,"gensim",s)
                
            
            
            
                
             
            
            
            y+=1
            
            row=row+1
        row=row+1
        print(i)
 


       



df9.to_csv('C:/Users/imart/Desktop/Duplicate/output_2nd.csv',index=False)
