About Code : Code to clean testing data for fasttext method for custom to standard ISQ project


------------------------------------------------------------------------------


import pandas as pd
import numpy as np
import re
from io import StringIO

df = pd.read_excel('C:/Users/imart/Desktop/custom to standard project/forpmcat 10573/exporttesting_data_for_10573.xlsx')


'''for i in range(0,26662):
 f = df.OPTIONS_DESC[i].isdigit()
'''



specials = ['!','/','-','$','#','@','^','*','(',')','+','%','\\','"',']','[','+','=','-','&','?','_','to']
for i in range(0,len(df)):
  
    
    list1=[]
    
    if(type(df.OPTIONS_DESC[i]) == float):
        df.set_value(i,'OPTIONS_DESC','')
        continue
    
    list1 = [item for item in df.OPTIONS_DESC[i].split() if item not in specials]
    
    str1=''.join(list1)
    
    
    if(str1.isdigit() == True):
    
            df.set_value(i,'OPTIONS_DESC','')
            continue
    str2 = ' '.join(list1)
    df.set_value(i,'OPTIONS_DESC',str2)
         
space =[]
for d in range(0,len(df)):
    
    if(type(df.MASTER_DESC[d]) == float):
        space.append(d)
        continue
        #df.set_value(i,'MASTER_DESC','')
df.drop(df.index[space],inplace=True)
df.reset_index(drop=True,inplace=True)
       



digit_data = []
for e in range(0,len(df)):
  emails = re.findall(r'[0-9]',df.MASTER_DESC[e])
  if(emails == []):
      continue
  else:
      digit_data.append(e)
df.drop(digit_data, inplace=True)
df.reset_index(drop=True,inplace=True)  
    





for p in range(0,len(df)):    
   if(df.OPTIONS_DESC[p].startswith("as per") or df.OPTIONS_DESC[p].startswith("As per") or df.OPTIONS_DESC[p] == "Text" or df.OPTIONS_DESC[p] == "text" or df.OPTIONS_DESC[p] == "any" or df.OPTIONS_DESC[p] == "Any"):
      df.set_value(p,'OPTIONS_DESC','') 



asperlist = []
for p in range(0,len(df)):    
   if(df.MASTER_DESC[p].startswith("as per") or df.MASTER_DESC[p].startswith("aas per") or df.MASTER_DESC[p] == "Text" or df.MASTER_DESC[p] == "text" or df.MASTER_DESC[p] == "any" or df.MASTER_DESC[p] == "Any"):
     # df.drop(df.index[p],inplace=True)
     asperlist.append(p)
      #df.reset_index(drop=True,inplace=True)
df.drop(df.index[asperlist],inplace=True)
df.reset_index(drop=True,inplace=True)
      

    
    
for k in range(0,len(df)):
    df.set_value(k,'OPTIONS_DESC',df.OPTIONS_DESC[k].strip())
    df.set_value(k,'MASTER_DESC',df.MASTER_DESC[k].strip())
  
    
for l in range(0,len(df)): 
    abc = df.OPTIONS_DESC[l]
    abc = abc.replace("-", " ")
    abc = abc.replace("/", " ")
    abc = abc.replace("!", " ")
    abc = abc.replace("#", " ")
    abc = abc.replace("@", " ")
    abc = abc.replace("$", " ")
    abc = abc.replace("%", " ")
    abc = abc.replace("^", " ")
    abc = abc.replace("&", " ")
    abc = abc.replace("*", " ")
    abc = abc.replace("(", " ")
    abc = abc.replace(")", " ")
    abc = abc.replace("+", " ")
    abc = abc.replace(";", " ")
    abc = abc.replace("{", " ")
    abc = abc.replace("}", " ")
    abc = abc.replace("[", " ")
    abc = abc.replace("]", " ")
    abc = abc.replace("\\", " ")
    abc = abc.replace(".", " ")
    abc = abc.replace(",", " ")
    abc = abc.replace("?", " ")
    abc = abc.replace("w x d x h","")
    abc = abc.replace(" x ", " ")
    abc = abc.replace("wxdxh","w d h")
    df.set_value(l,'OPTIONS_DESC',abc)
    abc = df.MASTER_DESC[l]
    abc = abc.replace("-", " ")
    abc = abc.replace("/", " ")
    abc = abc.replace("!", " ")
    abc = abc.replace("#", " ")
    abc = abc.replace("@", " ")
    abc = abc.replace("$", " ")
    abc = abc.replace("%", " ")
    abc = abc.replace("^", " ")
    abc = abc.replace("&", " ")
    abc = abc.replace("*", " ")
    abc = abc.replace("(", " ")
    abc = abc.replace(")", " ")
    abc = abc.replace("+", " ")
    abc = abc.replace(";", " ")
    abc = abc.replace("{", " ")
    abc = abc.replace("}", " ")
    abc = abc.replace("[", " ")
    abc = abc.replace("]", " ")
    abc = abc.replace("\\", " ")
    abc = abc.replace(".", " ")
    abc = abc.replace("?", " ")
    abc = abc.replace(",", " ")
    abc = abc.replace("to", " ")
    abc = abc.replace("w x d x h","")
    abc = abc.replace(" x ", " ")
    abc = abc.replace("wxdxh","w d h")
    df.set_value(l,'MASTER_DESC',abc)
    
for r in range(0,len(df)):
    emails = re.findall(r'[0-9]x[0-9]',df.OPTIONS_DESC[r])
    if(emails == []):
        continue
    else:
        df.set_value(r,'OPTIONS_DESC',df.OPTIONS_DESC[r].replace("x", " "))
for r in range(0,len(df)):
    emails = re.findall(r'[0-9]x[0-9]',df.MASTER_DESC[r])
    if(emails == []):
        continue
    else:
        df.set_value(r,'MASTER_DESC',df.MASTER_DESC[r].replace("x", " "))
    
    


for s in range(0,len(df)):
    emails = re.findall(r' x[0-9]',df.OPTIONS_DESC[s])
    if(emails == []):
        continue
    else:
        df.set_value(s,'OPTIONS_DESC',df.OPTIONS_DESC[s].replace("x", " "))
for s in range(0,len(df)):
    emails = re.findall(r' x[0-9]',df.MASTER_DESC[s])
    if(emails == []):
        continue
    else:
        df.set_value(s,'MASTER_DESC',df.MASTER_DESC[s].replace("x", " "))
 
    
    
      
for t in range(0,len(df)):
    emails = re.findall(r'[0-9]x ',df.OPTIONS_DESC[t])
    if(emails == []):
        continue
    else:
        df.set_value(t,'OPTIONS_DESC',df.OPTIONS_DESC[t].replace("x", " "))
for t in range(0,len(df)):
    emails = re.findall(r'[0-9]x ',df.MASTER_DESC[t])
    if(emails == []):
        continue
    else:
        df.set_value(t,'MASTER_DESC',df.MASTER_DESC[t].replace("x", " "))
    
   
    
    
for m in range(0,len(df)):
   df.set_value(m,'OPTIONS_DESC', df.OPTIONS_DESC[m].replace("    ", " "))
   df.set_value(m,'OPTIONS_DESC', df.OPTIONS_DESC[m].replace("   ", " "))
   df.set_value(m,'OPTIONS_DESC', df.OPTIONS_DESC[m].replace("  ", " "))
   df.set_value(m,'MASTER_DESC', df.MASTER_DESC[m].replace("   ", " "))
   df.set_value(m,'MASTER_DESC', df.MASTER_DESC[m].replace("  ", " "))
   df.set_value(m,'MASTER_DESC', df.MASTER_DESC[m].replace("__", " "))
   df.set_value(m,'MASTER_DESC', df.MASTER_DESC[m].replace("___", " "))
   
   

   
   
for n in range(0,len(df)):
    df.set_value(n,'OPTIONS_DESC',df.OPTIONS_DESC[n].strip())
    df.set_value(n,'MASTER_DESC',df.MASTER_DESC[n].strip())
    
    
again = []
for p in range(0,len(df)):    
   if(df.MASTER_DESC[p].startswith("as per") or df.MASTER_DESC[p].startswith("aas per") or df.MASTER_DESC[p] == "Text" or df.MASTER_DESC[p] == "text" or df.MASTER_DESC[p] == "any" or df.MASTER_DESC[p] == "Any"):
     # df.drop(df.index[p],inplace=True)
     asperlist.append(p)
      #df.reset_index(drop=True,inplace=True)
df.drop(df.index[again],inplace=True)
df.reset_index(drop=True,inplace=True)
      
space =[]
for d in range(0,len(df)):
    
    if(df.MASTER_DESC[d] == ""):
        space.append(d)
        continue
        #df.set_value(i,'MASTER_DESC','')
df.drop(df.index[space],inplace=True)
df.reset_index(drop=True,inplace=True)




df.drop(column = ['MASTER_DESC||''||','||''||OPTIONS_DESC']) 
df["Join"] = ""
 
for u in range(0,len(df)):
   print(u)
   df.Join[u]="".join([df.MASTER_DESC[u],' , ',df.OPTIONS_DESC[u]])


df.columns.value  
df2=pd.DataFrame(columns={'MCAT_ID','Join'})
df2['MCAT_ID']=df['MCAT_ID']
df2['GLCAT_MCAT_NAME']=df['GLCAT_MCAT_NAME']
df2['MASTER_DESC']=df['MASTER_DESC']
df2['OPTIONS_DESC']=df['OPTIONS_DESC']
df2['Join']=df['Join']






df.to_excel('C:/Users/imart/Desktop/custom to standard project/forpmcat 10573/Testing_Data_Standard_ISQ_After_Cleaning.xlsx',index=False)



df2 = df2.drop_duplicates()
df2.reset_index(drop=True,inplace=True)



    
for z in range(0,len(df2)):
       text_file = open("C:/Users/imart/Desktop/custom to standard project/forpmcat 10573/Output_testing/"+str(df2.MCAT_ID[z]) + ".txt", "a")
       text_file.write(df2.Join[z])
       text_file.write('\n')
       text_file.close()
