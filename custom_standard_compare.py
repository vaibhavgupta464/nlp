About Code : Code to compare custom and standard ISQ using gensim method



------------------------------------------------------------------------------



import gensim
model = gensim.models.KeyedVectors.load_word2vec_format('C:/Users/imart/Downloads/GoogleNews-vectors-negative300.bin', binary=True) # requires 3 gb data to download
'''
try:
    s = model.similarity('diameter','width')
except:
    continue

'''


import pandas as pd

df1 = pd.read_csv('file:///C:/Users/imart/Desktop/rasika custom standard/Custom_ISQ_after_cleaning.csv') #custom

df2 = pd.read_csv('file:///C:/Users/imart/Desktop/rasika custom standard/Standard_ISQ_Final.csv')  #standard

df2.columns

df1.drop('Unnamed: 0', axis = 1, inplace = True)
df2.drop('Unnamed: 0', axis = 1, inplace = True)
df2.drop('MCAT_NAME', axis = 1, inplace = True)
df2.drop('ISQ_1', axis = 1, inplace = True)
df2.drop('Value', axis = 1, inplace = True)


 
for n in range(0,len(df1)):
    df1.set_value(n,'ISQ',df1.ISQ[n].strip())
    df1.set_value(n,'ISQ',df1.ISQ[n].strip())
    print(n)
    
for m in range(0,len(df2)):
    df2.set_value(m,'ISQ',df2.ISQ[m].strip())
    df2.set_value(m,'ISQ',df2.ISQ[m].strip())    
    

df101 = df1.head(365)
df102 = df2.head(140)



    
df9=pd.DataFrame(columns=['mcat','custom isq','standard isq','gensim','similarity','method'])
i=0
j=0
mcatslist=list(df1['MCAT_ID'].unique())
mcatslist1=list(df2['MCAT_ID'].unique())
row=0

for i in mcatslist:
    isqlist=list(df1[df1['MCAT_ID']==i]['ISQ'].values)
    isqlist1=list(df2[df2['MCAT_ID']==i]['ISQ'].values)
    
    for j in isqlist:
        y=1
        for x in isqlist1:
            wordFromList1=[]
            wordFromList2=[]
            
            try:
                percentage=0
                s = model.similarity(j.lower(),x.lower())
                if(s>=0.53):
                    df9.set_value(row,'mcat',i)
                    df9.set_value(row,"custom isq",j)
                    df9.set_value(row,"standard isq",x)
                    df9.set_value(row,"method",'gensim method')
                    df9.set_value(row ,"similarity",percentage)
                    df9.set_value(row ,"gensim",s)
                    
                
            except:
                s=0
                matched_characters = sum(c1 == c2 for c1, c2 in zip(j.lower(),x.lower()))
                if(len(j) > len(x)):
                    percentage = matched_characters/len(j)
                    
                else:
                    percentage = matched_characters/len(x)
                    
                if(percentage>=0.85):
                    df9.set_value(row,'mcat',i)
                    df9.set_value(row,"custom isq",j)
                    df9.set_value(row,"standard isq",x)
                    df9.set_value(row,"method",'Simlarity Method')
                    df9.set_value(row ,"similarity",percentage)
                    df9.set_value(row ,"gensim",s)
                    
            
            
            
            y+=1
            
            row=row+1
        row=row+1
        print(i)
 

df9.to_excel('C:/Users/imart/Desktop/Duplicate/custom_std_output.xlsx',index=False)







       
df10 = df9
df10['Flag']= df10['gensim']>=0.53
df10=df10[df10['Flag']]
df10.drop('similarity', axis = 1, inplace = True)
df10.drop('Flag', axis = 1, inplace = True)
df10.rename(columns={'gensim':"match_percentage"}, inplace=True)
df10.method ='gensim method'

df9.drop('gensim', axis = 1, inplace = True)


df9['Flag']= df9['similarity']>=0.85
df9=df9[df9['Flag']]

df9.drop('Flag', axis = 1, inplace = True)

df9.rename(columns={'similarity':"match_percentage"}, inplace=True)
df9.method ='Similarity method'

df9=pd.concat([df9,df10],ignore_index=False)
df9 = df9.sort_values(by=['mcat'])

df100 = df9
df101 = df1
df102 = df2




df9.to_excel('C:/Users/imart/Desktop/Duplicate/custom_std_output.xlsx',index=False)


and




df1 = pd.read_excel('file:///C:/Users/imart/Downloads/Custom to standard for mcat having less than 6 ISQs (2).xlsx') #custom

df2 = pd.read_excel('file:///C:/Users/imart/exportcustomtostandard.xlsx')  #standard


df9=pd.DataFrame(columns=['mcat','custom isq','standard isq','similarity','method'])

i=0
j=0
mcatslist=list(df1['mcat'].unique())
mcatslist1=list(df2['mcat'].unique())
row=0


#x=1
for i in mcatslist:
    isqlist=list(df1[df1['mcat']==i]['isq'].values)
    isqlist1=list(df2[df2['mcat']==i]['isq'].values)
    for j in isqlist:
        y=1
        for x in isqlist1:
            wordFromList1=[]
            wordFromList2=[]
            
            try:
                percentage=0
                s = model.similarity(isqlist[j].lower(),isqlist[j+y].lower())
                    
                    
                    
                
            except:
                s=0
                if(  j.count(' ')>0 or x.count(' ')>0 ):
                    ratio = SequenceMatcher(None,j.lower(),x.lower()).ratio()
                    
                        
                    if(ratio>=0.7):
                        df9.set_value(row,'mcat',i)
                        df9.set_value(row,"custom isq",j)
                        df9.set_value(row,"standard isq",x)
                        df9.set_value(row,"method",'Simlarity Method')
                        df9.set_value(row ,"similarity",ratio)
                        
                
            
            
            
                
             
            
            
            y+=1
            
            row=row+1
        row=row+1
        print(i)
